<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">

#### SERVLETS
![](../ILLUSTRATIONS/servlets.png)

#### doGet()
![](../ILLUSTRATIONS/doget.png)
1. extract params from request
2. validation
3. construct objects with params
4. do work.

#### doPost()
![](../ILLUSTRATIONS/dopost.png)

doPost() is more complicated. So Spring is used to enter more abstraction. Basically the error checking parts.

#### webxml
routing to appropriate servlet based on the path sent by the client.
![](../ILLUSTRATIONS/dopost.png)

#### INJECTION ATTACKS
Anything that is coming from client, you need to do enough of error checking to avoid the attacks.

#### BUILDING CLOUD SERVICES
![](../ILLUSTRATIONS/cloudservicehttp.png)

#### Spring annotations
1. We are write controllers which are normal Java class with public methods. My Car, Bike classes were controllers. In that there were public methods which needs to be mapped using this annotation. Each public method will have an annotation -
```Java
@Controller
public class Contactsctrl{
  Contacts c;
  @RequestMapping("/contacts")
  public Contacts get_contact(){
    return new Contacts();
  }
}

@RequestMapping
```
#### Links to the annotation videos. 
https://coursera.org/share/52921ccb12dd81416f7645172051e744
https://coursera.org/share/73a928cd772bef7231f9a7b597814fa2
https://coursera.org/share/676a6ef1f45c30d70929bc9714c5510d


#### QUIZ
**1. Question 1 - What is the purpose of request routing? 1 point**
- [ ] to determine which mime type the request should be interpreted as
- [ ] to determine which client should receive the response
- [x] to determine which function should generate the response for a given request
- [ ] to determine which header should be used to authenticate the user

**2. Question 2 - Which of the following are true of client request data? 1 point**
- [ ] the data will be in the correct format or the HTTP server would reject it
- [x] the data could lead to an injection attack
- [ ] the data will be in the correct format due to Spring annotations
- [x] the data should be carefully sanitized and validated

**3. Question 3 How is HTTP used in cloud services? 1 point**
- [x] HTTP can be used as the communication protocol for talking to cloud services
- [ ] HTTP provides the data format used inside of services
- [ ] HTTP cookies provide the security for cloud applications
- [x] HTTP headers provide routing information for cloud commands

**4. Question 4 What is the relationship between a Spring controller and the dispatcher servlet? 1 point**
- [ ] controllers inherit from the dispatcher servlet
- [ ] controllers provide the dispatcher servlet with access to a database
- [x] the dispatcher servlet routes HTTP requests to one or more controllers to produce a response
- [ ] the dispatcher servlet guarantees the security of controllers

**5. Question 5 Which of the following are true? 1 point**
- [x] The RequestBody annotation is used to indicate that a method parameter should be bound to the body of an HTTP request
- [x] The RequestParam annotation is used to indicate that a method parameter should be bound to a specific parameter from the HTTP request
- [x] The PathVariable annotation is used to indicate that a method parameter should be bound to a component of the path the HTTP request was sent to
