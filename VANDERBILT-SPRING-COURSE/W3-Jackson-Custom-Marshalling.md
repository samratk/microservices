```java
public class Video{

}
```
#### Code walkthrough
https://coursera.org/share/9f197964677cf06aee75e7dbfee574ab

#### Horizontal scaling
1. Make the serverlets stateless application. This helps to have clients be routed to any server without any setup of client information.
2. State - need to bootstrap correctly.

#### Load balancing
1. Have the application class instantiated in different servers.
2. Have clients allocated to the machine based on the availablity.
   1. Round robin - assumes that all servers are `STATELESS`. Routing is easy.
   2. `STICKY SESSIONS` - If the requests are stateful, Rather than routing on the request level, route on the basis of individual phones. This will allow the state information to be stored in the allocated servers. And only those servers are routed to the respective clients.
   3. `DISTRIBUTED STATE`- Have a centralized data base from which the state is accessed from that database layer by the servers.
