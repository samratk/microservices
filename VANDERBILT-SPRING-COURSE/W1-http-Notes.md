<span style="color:; font-family:Menlo, Consolas, DejaVu Sans Mono, monospace; font-size: 12pt ;">
### PROTOCOLS
1. Syntax
2. Semantics
3. Timing. 
### HTTP
1. HTTP is a client server protocol. So the communications start from client requesting an information from the server. Server has a pool of resources from where it serves the client. Client starts the interaction. Server processes the interaction.
2. Advantages of HTTP
   1. The server is client agnostic. The requests from clients are same irrespective of whether the client is a mobile device or a browser. `COMMON INTERFACE BW CLIENT AND SERVER`
   2. Resource in servers that can be re-used
      1. Load balancing
      2. Session infrastructure
      3. Data Marshaling
      4. Resource
3. Issues with Http - When you want the transaction to be initiated from the server it is not possible. We have workarounds.
2. CLOUD SERVICES
1. inputs and output is via request and respond json requests. It is remote interaction based on commands. Not a vanilla input and output.

### HTTP REQUEST METHODS
1. get() - may be with data or without data input.
2. post() - when you want to send a lot of data to the server. Adds data
3. put() - asking the server to store some data to the server that is being passed. Replaces the data with the new data being sent.
4. delete() - delete some resource in server.

### HTTP REQUEST ANATOMY
1. Request line - URL - from `client`
   1. request method
   2. resource path
   3. headers - extra information to help the server.
      1. Language of the response expected
      2. character set that we would like to see the response in
      3. content type that is expected.
      4. cookies.
   4. Body (optional)
   5. Query parameters.
   6. Content type header via MIME types  - from server or client request or response that includes a body - the format of the body is described
      1. image/jpeg
      2. image/png
      3. text/plain
      4. text/html
2. Request encoding - Send data to server.
   1. key value pair in URL - does not make sense always
   2. send data in the body
      1. url encoded - small amount of data. simple key value pairs.
      2. multi part - binary data, more amount of data
      3. application/json
3. Response anatomy - from `server` to client
   1. Status Line
      1. Response code
      2. Phrase or text
   2. Headers
       1. Content type
       2. Cookies
   3. Body
4. HTTP response codes - client app should handle the responses differently.
   1. 1XXX - Informational
   2. 2XXX - Successful
   3. 3XXX - Re-direction - client has to do some extra work to send the request
   4. 4XXX - Client error
      1. 404 - resource was not found.
   5. 5XXX - Server error
      1. 500 - some error in server.
### COOKIES
1.  Small amount of data that the server sends to the client so that it can store the same and then send it to the server when it requires it.
2. There are different qualifiers that are send with cookies which helps clients to make runtime decisions how to handle the cookies.

### PROTOCOL LAYERING HTTP DESIGN METHODOLOGIES
1. WSDL
2. SOAP
3. REST

### HTTP POLLING
1. Client regularly polls the server after every t interval.
2. 2t interval
3. 4t (exponential increase in time)
4. `websocket` - the http is upgraded to websocket. now server can send data by themselves. websocket will not work if clients are not connected online. client need to reconnect and upgrade to websocket. this reconnect error handling is required for websocket. this will also alleviate usage of http mandatory fields. so data transfer is more efficient. The downside is that client need to do error handling.
5. websocket has a persistent connect with the client. This hogs the server capability.

### REST
1. Interacting with resources.
2. Interaction are specified with URL to access these resources.
3. A part of full data can be accessed using the same put, get, post

### PUSH MESSAGING - SERVER TO CLIENT
1. GCM - Google cloud messaging framework.
   1. Every android client automatically sets up a persistent connection to the GCM servers. This connection is done over XMPP - XML based messaging protocol. This enables it to push information to the phone anytime.
      1. Android connects to GCM
      2. Gets a registration id from GCM
      3. GCM takes care of the auto connection with the client.
      4. If a 3rd server wants to send a message to the client, rather than sending to the client, it can send it to the GCM using the registration id of the client. GCM takes care of the pushing of the message later. <4KB
      5. Push to synch model
      6.
