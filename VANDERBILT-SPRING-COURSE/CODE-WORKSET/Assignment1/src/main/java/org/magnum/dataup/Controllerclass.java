package org.magnum.dataup;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;


import java.io.OutputStream;

import java.util.ArrayList;

import java.util.Collection;

import java.util.HashMap;

import java.util.List;

import java.util.Map;

import java.util.concurrent.atomic.AtomicLong;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpResponse;

import org.apache.http.HttpStatus;

import org.magnum.dataup.model.Video;

import org.magnum.dataup.model.VideoStatus;

import org.magnum.dataup.model.VideoStatus.VideoState;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestBody;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.web.bind.annotation.ResponseBody;

import org.springframework.web.context.request.RequestContextHolder;

import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

//import org.springframework.web.multipart.MultipartFile;

import retrofit.mime.TypedFile;

@Controller

public class Controllerclass {

	private static final AtomicLong currentId = new AtomicLong(0L);
	private List<Video> list=new ArrayList();
	private Map<Long,Video>data=new HashMap<Long,Video>();
	
	private void checkAndSetId(Video entity) {
	if(entity.getId() == 0){
		entity.setId(currentId.incrementAndGet());

	}

	}


	public Video addvideo(Video entity) {
		checkAndSetId(entity);
		entity.setDataUrl(getDataUrl(entity.getId()));
		list.add(entity);
		//String url=getDataUrl(entity.getId());
		//entity.addLink(url,"self");
		data.put(entity.getId(), entity);
		return entity;
	}

	private String getDataUrl(long videoId){
		String url = getUrlBaseForLocalServer() + "/video/" + videoId + "/data";
		return url;
		}

	private String getUrlBaseForLocalServer() {
		HttpServletRequest request =
				((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String base = "http://"+request.getServerName() + ((request.getServerPort() != 80) ? ":"+request.getServerPort() : "");
		return base;

	}

	@RequestMapping(value="/video",method= RequestMethod.GET)
	public Collection<Video> getVideo()	{

		return list;
	}

	@RequestMapping(value="/video",method=RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v){
		return addvideo(v);

	}

	@RequestMapping(value="/video/{id}/data",method=RequestMethod.POST)
	public VideoState update(@PathVariable("id") long id,
		@RequestParam("data") MultipartFile videoData,
		HttpServletResponse response) throws IOException {

		//int id1=Integer.parseInt(id);
		Video video=list.get((int)id);
		response.setHeader("Content-Type","application/json");
		if(video==null){
			return VideoStatus.VideoState.PROCESSING;
		}
		else{
			VideoFileManager.get().saveVideoData(video, videoData.getInputStream());
			return VideoStatus.VideoState.READY;
		}

		//return response;
	}


	@RequestMapping(value = "video/{id}/data", method = RequestMethod.GET)
	public VideoState getData(@PathVariable("id") long videoId, HttpServletResponse response)
			throws IOException {
		Video video = list.get((int)videoId);
		response.setHeader("Content-Type", "application/json");
		OutputStream out = response.getOutputStream();
		// response.setStatus(HttpStatus.);
		VideoFileManager.get().copyVideoData(video, out);
		out.flush();
		out.close();
		return VideoStatus.VideoState.READY;
		}	
	}